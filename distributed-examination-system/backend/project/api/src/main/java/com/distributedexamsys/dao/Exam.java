package com.distributedexamsys.dao;



import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
 
@Entity
public class Exam {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int exam_id;
	
	@Column
	private String exam_name;
	
	@Column
	private Date date; 
	
	@Column
	private String time;
	
	@Column
	private int module_module_id;

	public int getExam_id() {
		return exam_id;
	}

	public void setExam_id(int exam_id) {
		this.exam_id = exam_id;
	}

	public String getExam_name() {
		return exam_name;
	}

	public void setExam_name(String exam_name) {
		this.exam_name = exam_name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getModule_module_id() {
		return module_module_id;
	}

	public void setModule_module_id(int module_module_id) {
		this.module_module_id = module_module_id;
	}
	
	
}
