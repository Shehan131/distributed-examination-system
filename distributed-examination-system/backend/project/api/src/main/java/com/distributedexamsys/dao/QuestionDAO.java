package com.distributedexamsys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class QuestionDAO {
	
	 public void addQuestion(Question bean){
	        Session session = SessionUtil.getSession();        
	        Transaction tx = session.beginTransaction();
	        
	        addQuestion(session,bean);        
	        tx.commit();
	        session.close();
	        
	    }

	private void addQuestion(Session session, Question bean) {
		// TODO Auto-generated method stub
		Question question = new Question();
		question.setQuestion(bean.getQuestion());
		question.setExam_exam_id(bean.getExam_exam_id());
		question.setAdmin_admin_id(bean.getAdmin_admin_id());
		
		session.save(question);
	}
	
	//return questions for a selected exam
	public List<Question> getQuestions(int exam_exam_id){
		Session session = SessionUtil.getSession();
		String hql = "from Question where exam_exam_id = :exam_exam_id";
		
		Query query = session.createQuery(hql);
		query.setInteger("exam_exam_id", exam_exam_id);
		List<Question> questions = query.list();
		session.close();
		return questions;
	}
	
	public int deleteQuestion(int question_id) {
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        
        String hql = "delete from Question where question_id = :question_id";
        Query query = session.createQuery(hql);
        query.setInteger("question_id",question_id);
        int rowCount = query.executeUpdate();
        tx.commit();
        session.close();
        return rowCount;
    }
	
	public int updateQuestion(int question_id, Question question){
        if(question_id <=0)  
              return 0;  
        Session session = SessionUtil.getSession();
           Transaction tx = session.beginTransaction();
           
           String hql = "update Question set question = :question, exam_exam_id=:exam_exam_id , admin_admin_id = :admin_admin_id  where question_id = :question_id";
           Query query = session.createQuery(hql);
           
           query.setString("question", question.getQuestion());
           query.setInteger("exam_exam_id", question.getExam_exam_id());
           query.setInteger("admin_admin_id", question.getAdmin_admin_id());
           query.setInteger("question_id", question_id);
           
           int rowCount = query.executeUpdate();
           tx.commit();
           session.close();
           return rowCount;
   }
}
