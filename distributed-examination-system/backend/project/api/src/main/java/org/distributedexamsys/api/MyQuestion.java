package org.distributedexamsys.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;


import com.distributedexamsys.dao.Question;
import com.distributedexamsys.dao.QuestionDAO;



@Path("/questions")
public class MyQuestion {
	
	@GET
	@Path("/filter")
	@Produces("application/json")
	public List<Question> getQuestions(@QueryParam("exam_exam_id") int exam_exam_id ){
		
		QuestionDAO dao = new QuestionDAO();
        List<Question> question = dao.getQuestions(exam_exam_id);
        return question;
	}
	
	@POST
    @Path("/create")
    @Consumes("application/json")
    public Response addExam(Question question){
        
		question.setQuestion(question.getQuestion());
		question.setExam_exam_id(question.getExam_exam_id());
		question.setAdmin_admin_id(question.getAdmin_admin_id());
                
        QuestionDAO dao = new QuestionDAO();
        dao.addQuestion(question);
        
        return Response.ok().build();
    }
	
	@PUT
    @Path("/update/{question_id}")
    @Consumes("application/json")
    public Response updateQuestion(@PathParam("question_id") int question_id , Question question){
        QuestionDAO dao = new QuestionDAO();
        int count = dao.updateQuestion(question_id, question);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
	
	
	@DELETE
    @Path("/delete/{question_id}")
    @Consumes("application/json")
    public Response deleteQuestion(@PathParam("question_id") int question_id){
		QuestionDAO dao = new QuestionDAO();
		
        int count = dao.deleteQuestion(question_id);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
	
}
