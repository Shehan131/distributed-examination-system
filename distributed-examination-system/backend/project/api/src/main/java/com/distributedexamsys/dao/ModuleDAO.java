package com.distributedexamsys.dao;



import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class ModuleDAO {

	public void addModule(Module bean){
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		addModule(session , bean);
		tx.commit();
		session.close();
		
	}

	private void addModule(Session session, Module bean) {
		// TODO Auto-generated method stub
		Module module = new Module();
		
		module.setName(bean.getName());
		session.save(module);
		
	}
	
	
	public List<Module> getModules(){
		Session session = SessionUtil.getSession();
		Query query = session.createQuery("from Module");
		List<Module> modules = query.list();
		return modules;
	}
	
	
	public int updateModule(int module_id , Module module){
		if(module_id <=0){
			return 0;
		}
		
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "update Module set name = :name where module_id = :module_id";
		Query query = session.createQuery(hql);
		query.setInteger("module_id", module_id);
		query.setString("name", module.getName());
		int rowCount = query.executeUpdate();
		tx.commit();
		session.close();
		return rowCount;
		
	}
	
	
	public int deleteModule(int module_id){
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "delete from Module where module_id = :module_id";
		Query query = session.createQuery(hql);
		query.setInteger("module_id", module_id);
		int rowCount = query.executeUpdate();
		tx.commit();
		session.close();
		return rowCount;
		
	}
	
	
}
