package org.distributedexamsys.api;

import java.awt.PageAttributes.MediaType;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;


import com.distributedexamsys.dao.Student;
import com.distributedexamsys.dao.StudentDAO;
/**
 * Root resource (exposed at "myresource" path)
 */
@Path("/student")
public class MyResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces("application/json")
    
    public List<Student> getStudent() {
        StudentDAO dao = new StudentDAO();
        List students = dao.getStudents();
        return students;
    }
}
