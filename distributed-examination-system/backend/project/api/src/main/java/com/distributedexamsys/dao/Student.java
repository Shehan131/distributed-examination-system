package com.distributedexamsys.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Student{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int student_id;
	@Column
	private String full_name;
	@Column
	private String username;
	@Column
	private String password;
	
	
	public int getStudent_id() {
		return student_id;
	}
	
	
	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}
	
	
	public String getFull_name() {
		return full_name;
	}
	
	
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	
	
	public String getUsername() {
		return username;
	}
	
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	public String getPassword() {
		return password;
	}
	
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
