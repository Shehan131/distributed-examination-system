package org.distributedexamsys.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.distributedexamsys.dao.Answer;
import com.distributedexamsys.dao.AnswerDAO;




@Path("/answers")
public class MyAnswer {
	
	@GET
	@Path("/filter")
	@Produces("application/json")
	public List<Answer> getAnswer(@QueryParam("question_question_id") int question_question_id ){
		
		AnswerDAO dao = new AnswerDAO();
        List<Answer> answer = dao.getAnswer(question_question_id);
        return answer;
	}
	
	@POST
    @Path("/create")
    @Consumes("application/json")
    public Response addExam(Answer answer){
        answer.setRial_answer(answer.getRial_answer());
        answer.setFake_1(answer.getFake_1());
        answer.setFake_2(answer.getFake_2());
        answer.setFake_3(answer.getFake_3());
        answer.setAdmin_admin_id(answer.getAdmin_admin_id());
        answer.setQuestion_question_id(answer.getQuestion_question_id());
                
        AnswerDAO dao = new AnswerDAO();
        dao.addAnswer(answer);
        
        return Response.ok().build();
    }
	
	@PUT
    @Path("/update/{answer_id}")
    @Consumes("application/json")
    public Response updateAnswer(@PathParam("answer_id") int answer_id , Answer answer){
        AnswerDAO dao = new AnswerDAO();
        int count = dao.updateAnswer(answer_id, answer);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
	
	@DELETE
    @Path("/delete/{answer_id}")
    @Consumes("application/json")
    public Response deleteAnswer(@PathParam("answer_id") int answer_id){
		AnswerDAO dao = new AnswerDAO();
        int count = dao.deleteAnswer(answer_id);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
}
