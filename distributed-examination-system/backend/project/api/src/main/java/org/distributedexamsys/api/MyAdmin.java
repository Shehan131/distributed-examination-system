package org.distributedexamsys.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.distributedexamsys.dao.Admin;
import com.distributedexamsys.dao.AdminDAO;

@Path("/admins")
public class MyAdmin {
	
	@GET()
	@Produces("application/json")
	public List<Admin> getAdmin(@QueryParam("username") String username){
		
		AdminDAO dao = new AdminDAO();
        List<Admin> admin = dao.getAdmin(username);
        return admin;
	}
	
	@POST
    @Path("/create")
    @Consumes("application/json")
    public Response addAdmin(Admin admin){
        admin.setName(admin.getName());
        admin.setUsername(admin.getUsername());
        admin.setPassword(admin.getPassword());
                
        AdminDAO dao = new AdminDAO();
        dao.addAdmin(admin);
        
        return Response.ok().build();
    }
	
	@PUT
    @Path("/update/{admin_id}")
    @Consumes("application/json")
    public Response updateAdmin(@PathParam("admin_id") int admin_id, Admin admin){
        AdminDAO dao = new AdminDAO();
        int count = dao.updateAdmin(admin_id, admin);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
	
	@DELETE
    @Path("/delete/{admin_id}")
    @Consumes("application/json")
    public Response deleteAdmin(@PathParam("admin_id") int admin_id){
        AdminDAO dao = new AdminDAO();
        int count = dao.deleteAdmin(admin_id);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
}
