package com.distributedexamsys.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Result {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int result_id;
	
	@Column
	private int result;
	
	@Column
	private int student_student_id;
	
	@Column
	private int exam_exam_id;

	public int getResult_id() {
		return result_id;
	}

	public void setResult_id(int result_id) {
		this.result_id = result_id;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getStudent_student_id() {
		return student_student_id;
	}

	public void setStudent_student_id(int student_student_id) {
		this.student_student_id = student_student_id;
	}

	public int getExam_exam_id() {
		return exam_exam_id;
	}

	public void setExam_exam_id(int exam_exam_id) {
		this.exam_exam_id = exam_exam_id;
	}

	
}
