package org.distributedexamsys.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.distributedexamsys.dao.Exam;
import com.distributedexamsys.dao.ExamDAO;

@Path("/exams")
public class MyExam {
	
	@GET
    @Produces("application/json")
	public List<Exam> getExams() {
        ExamDAO dao = new ExamDAO();
        List exams = dao.getExams();
        return exams;
    }
	
	@GET
	@Path("/filter")
	@Produces("application/json")
	public List<Exam> getExam(@QueryParam("module_module_id") int module_module_id ){
		
		ExamDAO dao = new ExamDAO();
        List<Exam> exam = dao.getExam(module_module_id);
        return exam;
	}
	
	@POST
    @Path("/create")
    @Consumes("application/json")
    public Response addExam(Exam exam){
        exam.setExam_name(exam.getExam_name());
        exam.setDate(exam.getDate());
        exam.setTime(exam.getTime());
        exam.setModule_module_id(exam.getModule_module_id());
                
        ExamDAO dao = new ExamDAO();
        dao.addExam(exam);
        
        return Response.ok().build();
    }
	
	@PUT
    @Path("/update/{exam_id}")
    @Consumes("application/json")
    public Response updateExam(@PathParam("exam_id") int exam_id , Exam exam){
        ExamDAO dao = new ExamDAO();
        int count = dao.updateExam(exam_id, exam);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
 
	@DELETE
    @Path("/delete/{exam_id}")
    @Consumes("application/json")
    public Response deleteExam(@PathParam("exam_id") int exam_id){
		ExamDAO dao = new ExamDAO();
        int count = dao.deleteExam(exam_id);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
}
