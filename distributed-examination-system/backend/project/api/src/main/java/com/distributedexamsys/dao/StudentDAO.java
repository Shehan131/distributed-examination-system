package com.distributedexamsys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class StudentDAO {

	public void addStudent(Student bean){
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		addStudent(session , bean);
		tx.commit();
		session.close();
		
	}

	private void addStudent(Session session, Student bean) {
		// TODO Auto-generated method stub
		Student student = new Student();
		student.setFull_name(bean.getFull_name());
		student.setUsername(bean.getUsername());
		student.setPassword(bean.getPassword());
		
		session.save(student);
	}
	
	public List<Student> getStudents(){
		Session session = SessionUtil.getSession();
		Query query = session.createQuery("from Student");
		List<Student> students = query.list();
		session.close();
		return students;
		
	}
}
