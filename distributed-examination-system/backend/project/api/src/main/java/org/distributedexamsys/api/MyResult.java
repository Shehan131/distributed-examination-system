package org.distributedexamsys.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;


import com.distributedexamsys.dao.Result;
import com.distributedexamsys.dao.ResultDAO;


@Path("/results")
public class MyResult {
	
	@GET
    @Produces("application/json")
    public List<Result> getResults() {
        ResultDAO dao = new ResultDAO();
        List results = dao.getResults();
        return results;
    }
	
	@GET
	@Path("/filter")
	@Produces("application/json")
	public List<Result> getResult(@QueryParam("student_student_id") int student_student_id , @QueryParam("exam_exam_id") int exam_exam_id ){
		
		ResultDAO dao = new ResultDAO();
        List<Result> result = dao.getResult(student_student_id, exam_exam_id);
        return result;
	}
	
	@POST
    @Path("/create")
    @Consumes("application/json")
    public Response addResult(Result result){
        result.setResult(result.getResult());
        result.setStudent_student_id(result.getStudent_student_id());
        result.setExam_exam_id(result.getExam_exam_id());
                
        ResultDAO dao = new ResultDAO();
        dao.addResult(result);
        
        return Response.ok().build();
    }
}
