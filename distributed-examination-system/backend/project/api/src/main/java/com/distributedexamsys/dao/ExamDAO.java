package com.distributedexamsys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class ExamDAO {
	 public void addExam(Exam bean){
	        Session session = SessionUtil.getSession();        
	        Transaction tx = session.beginTransaction();
	        addExam(session,bean);        
	        tx.commit();
	        session.close();
	        
	    }

	private void addExam(Session session, Exam bean) {
		// TODO Auto-generated method stub
		Exam exam = new Exam();
		exam.setExam_name(bean.getExam_name());
		exam.setModule_module_id(bean.getModule_module_id());
		exam.setDate(bean.getDate());
		exam.setTime(bean.getTime());
		
		session.save(exam);
		
	}
	
	//returns all exams
	public List<Exam> getExams(){
        Session session = SessionUtil.getSession();    
        Query query = session.createQuery("from Exam");
        List<Exam> exams =  query.list();
        session.close();
        return exams;
    }
	
	//returns exam(s) per module
	public List<Exam> getExam(int module_module_id){
		Session session = SessionUtil.getSession();
		String hql = "from Exam where module_module_id = :module_module_id";
		Query query = session.createQuery(hql);
		query.setInteger("module_module_id", module_module_id);
		List<Exam> exam = query.list();
		session.close();
		return exam;
	}
	
	public int deleteExam(int exam_id) {
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        
        String hql = "delete from Exam where exam_id = :exam_id";
        Query query = session.createQuery(hql);
        query.setInteger("exam_id",exam_id);
        int rowCount = query.executeUpdate();
        tx.commit();
        session.close();
        return rowCount;
    }
	
	public int updateExam(int exam_id, Exam exam){
        if(exam_id <=0)  
              return 0;  
        Session session = SessionUtil.getSession();
           Transaction tx = session.beginTransaction();
           
           String hql = "update Exam set exam_name = :exam_name, date=:date , time = :time , module_module_id = :module_module_id where exam_id = :exam_id";
           Query query = session.createQuery(hql);
           
           query.setInteger("exam_id", exam_id);
           query.setString("exam_name",exam.getExam_name());
           query.setDate("date", exam.getDate());
           query.setString("time", exam.getTime());
           query.setInteger("module_module_id", exam.getModule_module_id());
           
           int rowCount = query.executeUpdate();
           tx.commit();
           session.close();
           return rowCount;
   }
}
