package com.distributedexamsys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class AdminDAO {
	
	public void addAdmin(Admin bean){
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		addAdmin(session , bean);
		tx.commit();
		session.close();
	}

	private void addAdmin(Session session, Admin bean) {
		// TODO Auto-generated method stub
		Admin admin = new Admin();
		admin.setName(bean.getName());
		admin.setUsername(bean.getUsername());
		admin.setPassword(bean.getPassword());
		
		session.save(admin);
	}
	
	
	public List<Admin> getAdmin(String username){
		Session session = SessionUtil.getSession();
		String hql = "from Admin where username = :username";
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<Admin> admin = query.list();
		session.close();
		return admin;
	}
	
	public int deleteAdmin(int admin_id){
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "delete from Admin where admin_id = :admin_id";
		Query query = session.createQuery(hql);
		query.setInteger("admin_id", admin_id);
		int rowCount = query.executeUpdate();
		tx.commit();
		session.close();
		return rowCount;
		
	}
	
	public int updateAdmin(int admin_id , Admin admin){
		if(admin_id <=0){
			return 0;
		}
		
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "update Admin set name = :name , username = :username , password = :password where admin_id = :admin_id";
		Query query = session.createQuery(hql);
		query.setInteger("admin_id", admin_id);
		query.setString("name", admin.getName());
		query.setString("username", admin.getUsername());
		query.setString("password", admin.getPassword());
		int rowCount = query.executeUpdate();
		tx.commit();
		session.close();
		return rowCount;
		
		
	} 
	
}
