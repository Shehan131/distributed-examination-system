package org.distributedexamsys.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.distributedexamsys.dao.Module;
import com.distributedexamsys.dao.ModuleDAO;

@Path("/modules")
public class MyModule {

	@GET
	@Produces("application/json")
	public List<Module> getModule(){
		ModuleDAO dao = new ModuleDAO();
		List modules = dao.getModules();
		return modules;
	}
	
	@POST
	@Path("/create")
	@Consumes("application/json")
	public Response addModule(Module module){
		module.setName(module.getName());
		ModuleDAO dao = new ModuleDAO();
		dao.addModule(module);
		return Response.ok().build();
	}
	
	@PUT
	@Path("/update/{module_id}")
	@Consumes("application/json")
	public Response updateModule(@PathParam("module_id") int module_id , Module module){
		ModuleDAO dao = new ModuleDAO();
		int count = dao.updateModule(module_id, module);
		if(count == 0){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/delete/{module_id}")
	@Consumes("application/json")
	public Response deleteModule(@PathParam("module_id") int module_id){
		ModuleDAO dao = new ModuleDAO();
		int count = dao.deleteModule(module_id);
		if(count == 0){
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		return Response.ok().build();
	}
	
	
}
