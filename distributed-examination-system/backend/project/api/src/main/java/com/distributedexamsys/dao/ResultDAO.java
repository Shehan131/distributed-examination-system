package com.distributedexamsys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ResultDAO {
	
	public void addResult(Result bean){
        Session session = SessionUtil.getSession();        
        Transaction tx = session.beginTransaction();
        addResult(session,bean);        
        tx.commit();
        session.close();
        
    }

	private void addResult(Session session, Result bean) {
		// TODO Auto-generated method stub
		Result result = new Result();
		result.setResult(bean.getResult());
		result.setStudent_student_id(bean.getStudent_student_id());
		result.setExam_exam_id(bean.getExam_exam_id());
		
		session.save(result);
	}
	
	//get whole result set
	public List<Result> getResults(){
        Session session = SessionUtil.getSession();    
        Query query = session.createQuery("from Result");
        List<Result> results =  query.list();
        session.close();
        return results;
    }
	
	//filter specific result
	public List<Result> getResult(int student_student_id , int exam_exam_id){
		Session session = SessionUtil.getSession();
		String hql = "from Result where student_student_id = :student_student_id and exam_exam_id = :exam_exam_id";
		Query query = session.createQuery(hql);
		query.setInteger("student_student_id", student_student_id);
		query.setInteger("exam_exam_id", exam_exam_id);
		List<Result> result = query.list();
		session.close();
		return result;
	}
}
