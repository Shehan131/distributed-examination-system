package com.distributedexamsys.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Answer {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int answer_id;
	
	@Column
	private String rial_answer;
	
	@Column
	private String fake_1;
	
	@Column
	private String fake_2;
	
	@Column
	private String fake_3;
	
	@Column
	private int question_question_id;
	
	@Column
	private int admin_admin_id;

	public int getAnswer_id() {
		return answer_id;
	}

	public void setAnswer_id(int answer_id) {
		this.answer_id = answer_id;
	}

	public String getRial_answer() {
		return rial_answer;
	}

	public void setRial_answer(String rial_answer) {
		this.rial_answer = rial_answer;
	}

	public String getFake_1() {
		return fake_1;
	}

	public void setFake_1(String fake_1) {
		this.fake_1 = fake_1;
	}

	public String getFake_2() {
		return fake_2;
	}

	public void setFake_2(String fake_2) {
		this.fake_2 = fake_2;
	}

	public String getFake_3() {
		return fake_3;
	}

	public void setFake_3(String fake_3) {
		this.fake_3 = fake_3;
	}

	public int getQuestion_question_id() {
		return question_question_id;
	}

	public void setQuestion_question_id(int question_question_id) {
		this.question_question_id = question_question_id;
	}

	public int getAdmin_admin_id() {
		return admin_admin_id;
	}

	public void setAdmin_admin_id(int admin_admin_id) {
		this.admin_admin_id = admin_admin_id;
	}
	
	
}
