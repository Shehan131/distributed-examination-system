package com.distributedexamsys.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Question {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int question_id;
	
	@Column
	private String question;
	
	@Column
	private int exam_exam_id;
	
	@Column
	private int admin_admin_id;

	public int getQuestion_id() {
		return question_id;
	}

	public void setQuestion_id(int question_id) {
		this.question_id = question_id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public int getExam_exam_id() {
		return exam_exam_id;
	}

	public void setExam_exam_id(int exam_exam_id) {
		this.exam_exam_id = exam_exam_id;
	}

	public int getAdmin_admin_id() {
		return admin_admin_id;
	}

	public void setAdmin_admin_id(int admin_admin_id) {
		this.admin_admin_id = admin_admin_id;
	}
	
	
}
