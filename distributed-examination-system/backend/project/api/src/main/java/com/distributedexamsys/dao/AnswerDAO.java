package com.distributedexamsys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class AnswerDAO {
	public void addAnswer(Answer bean){
        Session session = SessionUtil.getSession();        
        Transaction tx = session.beginTransaction();
        addAnswer(session,bean);        
        tx.commit();
        session.close();
        
    }

	private void addAnswer(Session session, Answer bean) {
		// TODO Auto-generated method stub
		Answer answer = new Answer();
		answer.setRial_answer(bean.getRial_answer());
		answer.setFake_1(bean.getFake_1());
		answer.setFake_2(bean.getFake_2());
		answer.setFake_3(bean.getFake_3());
		answer.setAdmin_admin_id(bean.getAdmin_admin_id());
		answer.setQuestion_question_id(bean.getQuestion_question_id());
		
		session.save(answer);
		
	}
	
	//returns an answer set per question
	
	public List<Answer> getAnswer(int question_question_id){
		Session session = SessionUtil.getSession();
		String hql = "from Answer where question_question_id = :question_question_id";
		Query query = session.createQuery(hql);
		query.setInteger("question_question_id", question_question_id);
		List<Answer> answer = query.list();
		session.close();
		return answer;
	}
	
	public int deleteAnswer(int answer_id) {
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        
        String hql = "delete from Answer where answer_id = :answer_id";
        Query query = session.createQuery(hql);
        query.setInteger("answer_id",answer_id);
        int rowCount = query.executeUpdate();
        tx.commit();
        session.close();
        return rowCount;
    }
	
	public int updateAnswer(int answer_id, Answer answer){
        if(answer_id <=0)  
              return 0;  
        Session session = SessionUtil.getSession();
           Transaction tx = session.beginTransaction();
           
           String hql = "update Answer set rial_answer = :rial_answer, fake_1=:fake_1 , fake_2 = :fake_2 , fake_3 = :fake_3 , question_question_id = :question_question_id , admin_admin_id = :admin_admin_id where answer_id = :answer_id";
           Query query = session.createQuery(hql);
           
          query.setString("rial_answer", answer.getRial_answer());
           query.setString("fake_1", answer.getFake_1());
           query.setString("fake_2", answer.getFake_2());
           query.setString("fake_3", answer.getFake_3());
           query.setInteger("question_question_id", answer.getQuestion_question_id());
           query.setInteger("admin_admin_id", answer.getAdmin_admin_id());
           query.setInteger("answer_id", answer_id);
           
           int rowCount = query.executeUpdate();
           tx.commit();
           session.close();
           return rowCount;
   }
}
